/* DOCUMENT READY ------------------------------ */

$(document).ready(function(){

	// Start foundation
    $(document).foundation()
			   .foundation('reveal', {
	    		   						 close_on_background_click: false, 
	    		   						 animation_speed: 200,
	    		   						 closed: function(){ $('#modalContent').html(''); }
    		   						 });
	
});


/* AJAX START/STOP EVENTS ------------------------------ */

$(document).ajaxStart(function(){ $("#loading").show(); }).ajaxStop(function(){ $("#loading").hide(); });


/* AJAX THAT POSTS DATA AND NOT NECESSARILY RETURNS VISUAL DATA ------------------------------ */

function ajaxPost(url,data)
{

	$.ajax({ type: 'POST', url: url, data: data, dataType:'script' })
	/*.done(function() { alert("success"); }).fail(function(jqXHR, textStatus) { alert( "Request failed: " + textStatus ); }).always(function() { alert("complete"); });*/
}


/* AJAX THAT LOADS SOME CONTENT ------------------------------ */

function ajaxLoad(element,url,params)
{

	$(element).load(url, params, function(response, status, xhr) {
	
		if (status === 'error')
		{
		
			switch(xhr.status)
			{
			    case 404: 
			    	//ajaxLoad('#mainContent','error-404.html',''); 
			    	alert('Página não encontrada');
			    break;
			}
			
		}
		
	});
}


/* OPEN MODAL ------------------------------ */
	
function openModal(url,params,text,size)
{

	if (!size) size = 'medium';
	
	// If @url is not empty then we change the #modalContent value with the @url value
	if(url !== '')
	{
		ajaxLoad('#modalContent',url,params);
	}
	// If @text is not empty then we change the #modalContent value with the @text value
	else if(text !== '')
	{
		$('#modalContent').html(text);
	}
	
	// Define the size class
	$('#modal').addClass(size);
	
	// If both @url and @text are empty, then the #modalContent remains unchanged.
	// Now we just show de modal with the changed (or not) content
	$('#modal').foundation('reveal', 'open');
	
	// Initialize validation, masks, etc
	setTimeout(setFormPatterns,1000);
	
	// Remove form error messages and alert-boxes
	$('.bVErrMsgContainer, .alert-box').remove();	
}


/* MASKED INPUT ------------------------------ */

(function(e){function t(){var e=document.createElement("input"),t="onpaste";return e.setAttribute(t,""),"function"==typeof e[t]?"paste":"input"}var n,a=t()+".mask",r=navigator.userAgent,i=/iphone/i.test(r),o=/android/i.test(r);e.mask={definitions:{9:"[0-9]",a:"[A-Za-z]","*":"[A-Za-z0-9]"},dataName:"rawMaskFn",placeholder:"_"},e.fn.extend({caret:function(e,t){var n;if(0!==this.length&&!this.is(":hidden"))return"number"==typeof e?(t="number"==typeof t?t:e,this.each(function(){this.setSelectionRange?this.setSelectionRange(e,t):this.createTextRange&&(n=this.createTextRange(),n.collapse(!0),n.moveEnd("character",t),n.moveStart("character",e),n.select())})):(this[0].setSelectionRange?(e=this[0].selectionStart,t=this[0].selectionEnd):document.selection&&document.selection.createRange&&(n=document.selection.createRange(),e=0-n.duplicate().moveStart("character",-1e5),t=e+n.text.length),{begin:e,end:t})},unmask:function(){return this.trigger("unmask")},mask:function(t,r){var c,l,s,u,f,h;return!t&&this.length>0?(c=e(this[0]),c.data(e.mask.dataName)()):(r=e.extend({placeholder:e.mask.placeholder,completed:null},r),l=e.mask.definitions,s=[],u=h=t.length,f=null,e.each(t.split(""),function(e,t){"?"==t?(h--,u=e):l[t]?(s.push(RegExp(l[t])),null===f&&(f=s.length-1)):s.push(null)}),this.trigger("unmask").each(function(){function c(e){for(;h>++e&&!s[e];);return e}function d(e){for(;--e>=0&&!s[e];);return e}function m(e,t){var n,a;if(!(0>e)){for(n=e,a=c(t);h>n;n++)if(s[n]){if(!(h>a&&s[n].test(R[a])))break;R[n]=R[a],R[a]=r.placeholder,a=c(a)}b(),x.caret(Math.max(f,e))}}function p(e){var t,n,a,i;for(t=e,n=r.placeholder;h>t;t++)if(s[t]){if(a=c(t),i=R[t],R[t]=n,!(h>a&&s[a].test(i)))break;n=i}}function g(e){var t,n,a,r=e.which;8===r||46===r||i&&127===r?(t=x.caret(),n=t.begin,a=t.end,0===a-n&&(n=46!==r?d(n):a=c(n-1),a=46===r?c(a):a),k(n,a),m(n,a-1),e.preventDefault()):27==r&&(x.val(S),x.caret(0,y()),e.preventDefault())}function v(t){var n,a,i,l=t.which,u=x.caret();t.ctrlKey||t.altKey||t.metaKey||32>l||l&&(0!==u.end-u.begin&&(k(u.begin,u.end),m(u.begin,u.end-1)),n=c(u.begin-1),h>n&&(a=String.fromCharCode(l),s[n].test(a)&&(p(n),R[n]=a,b(),i=c(n),o?setTimeout(e.proxy(e.fn.caret,x,i),0):x.caret(i),r.completed&&i>=h&&r.completed.call(x))),t.preventDefault())}function k(e,t){var n;for(n=e;t>n&&h>n;n++)s[n]&&(R[n]=r.placeholder)}function b(){x.val(R.join(""))}function y(e){var t,n,a=x.val(),i=-1;for(t=0,pos=0;h>t;t++)if(s[t]){for(R[t]=r.placeholder;pos++<a.length;)if(n=a.charAt(pos-1),s[t].test(n)){R[t]=n,i=t;break}if(pos>a.length)break}else R[t]===a.charAt(pos)&&t!==u&&(pos++,i=t);return e?b():u>i+1?(x.val(""),k(0,h)):(b(),x.val(x.val().substring(0,i+1))),u?t:f}var x=e(this),R=e.map(t.split(""),function(e){return"?"!=e?l[e]?r.placeholder:e:void 0}),S=x.val();x.data(e.mask.dataName,function(){return e.map(R,function(e,t){return s[t]&&e!=r.placeholder?e:null}).join("")}),x.attr("readonly")||x.one("unmask",function(){x.unbind(".mask").removeData(e.mask.dataName)}).bind("focus.mask",function(){clearTimeout(n);var e;S=x.val(),e=y(),n=setTimeout(function(){b(),e==t.length?x.caret(0,e):x.caret(e)},10)}).bind("blur.mask",function(){y(),x.val()!=S&&x.change()}).bind("keydown.mask",g).bind("keypress.mask",v).bind(a,function(){setTimeout(function(){var e=y(!0);x.caret(e),r.completed&&e==x.val().length&&r.completed.call(x)},0)}),y()}))}})})(jQuery);


/* MASK MONEY ------------------------------ */

(function(e){"use strict";if(!e.browser){e.browser={};e.browser.mozilla=/mozilla/.test(navigator.userAgent.toLowerCase())&&!/webkit/.test(navigator.userAgent.toLowerCase());e.browser.webkit=/webkit/.test(navigator.userAgent.toLowerCase());e.browser.opera=/opera/.test(navigator.userAgent.toLowerCase());e.browser.msie=/msie/.test(navigator.userAgent.toLowerCase())}var t={destroy:function(){e(this).unbind(".maskMoney");if(e.browser.msie){this.onpaste=null}return this},mask:function(t){return this.each(function(){var n=e(this),r;if(typeof t==="number"){n.trigger("mask");r=e(n.val().split(/\D/)).last()[0].length;t=t.toFixed(r);n.val(t)}return n.trigger("mask")})},unmasked:function(){return this.map(function(){var t=e(this).val()||"0",n=t.indexOf("-")!==-1,r;e(t.split(/\D/).reverse()).each(function(e,t){if(t){r=t;return false}});t=t.replace(/\D/g,"");t=t.replace(new RegExp(r+"$"),"."+r);if(n){t="-"+t}return parseFloat(t)})},init:function(t){t=e.extend({prefix:"",suffix:"",affixesStay:true,thousands:",",decimal:".",precision:2,allowZero:false,allowNegative:false},t);return this.each(function(){function i(){var e=n.get(0),t=0,r=0,i,s,o,u,a;if(typeof e.selectionStart==="number"&&typeof e.selectionEnd==="number"){t=e.selectionStart;r=e.selectionEnd}else{s=document.selection.createRange();if(s&&s.parentElement()===e){u=e.value.length;i=e.value.replace(/\r\n/g,"\n");o=e.createTextRange();o.moveToBookmark(s.getBookmark());a=e.createTextRange();a.collapse(false);if(o.compareEndPoints("StartToEnd",a)>-1){t=r=u}else{t=-o.moveStart("character",-u);t+=i.slice(0,t).split("\n").length-1;if(o.compareEndPoints("EndToEnd",a)>-1){r=u}else{r=-o.moveEnd("character",-u);r+=i.slice(0,r).split("\n").length-1}}}}return{start:t,end:r}}function s(){var e=!(n.val().length>=n.attr("maxlength")&&n.attr("maxlength")>=0),t=i(),r=t.start,s=t.end,o=t.start!==t.end&&n.val().substring(r,s).match(/\d/)?true:false,u=n.val().substring(0,1)==="0";return e||o||u}function o(e){n.each(function(t,n){if(n.setSelectionRange){n.focus();n.setSelectionRange(e,e)}else if(n.createTextRange){var r=n.createTextRange();r.collapse(true);r.moveEnd("character",e);r.moveStart("character",e);r.select()}})}function u(e){var n="";if(e.indexOf("-")>-1){e=e.replace("-","");n="-"}return n+t.prefix+e+t.suffix}function a(e){var n=e.indexOf("-")>-1&&t.allowNegative?"-":"",r=e.replace(/[^0-9]/g,""),i=r.slice(0,r.length-t.precision),s,o,a;i=i.replace(/^0*/g,"");i=i.replace(/\B(?=(\d{3})+(?!\d))/g,t.thousands);if(i===""){i="0"}s=n+i;if(t.precision>0){o=r.slice(r.length-t.precision);a=(new Array(t.precision+1-o.length)).join(0);s+=t.decimal+a+o}return u(s)}function f(e){var t=n.val().length,r;n.val(a(n.val()));r=n.val().length;e=e-(t-r);o(e)}function l(){var e=n.val();n.val(a(e))}function c(){var e=n.val();if(t.allowNegative){if(e!==""&&e.charAt(0)==="-"){return e.replace("-","")}else{return"-"+e}}else{return e}}function h(e){if(e.preventDefault){e.preventDefault()}else{e.returnValue=false}}function p(t){t=t||window.event;var r=t.which||t.charCode||t.keyCode,o,u,a,l,p;if(r===undefined){return false}if(r<48||r>57){if(r===45){n.val(c());return false}else if(r===43){n.val(n.val().replace("-",""));return false}else if(r===13||r===9){return true}else if(e.browser.mozilla&&(r===37||r===39)&&t.charCode===0){return true}else{h(t);return true}}else if(!s()){return false}else{h(t);o=String.fromCharCode(r);u=i();a=u.start;l=u.end;p=n.val();n.val(p.substring(0,a)+o+p.substring(l,p.length));f(a+1);return false}}function d(e){e=e||window.event;var r=e.which||e.charCode||e.keyCode,s,o,u,a,l;if(r===undefined){return false}s=i();o=s.start;u=s.end;if(r===8||r===46||r===63272){h(e);a=n.val();if(o===u){if(r===8){if(t.suffix===""){o-=1}else{l=a.split("").reverse().join("").search(/\d/);o=a.length-l-1;u=o+1}}else{u+=1}}n.val(a.substring(0,o)+a.substring(u,a.length));f(o);return false}else if(r===9){return true}else{return true}}function v(){r=n.val();l();var e=n.get(0),t;if(e.createTextRange){t=e.createTextRange();t.collapse(false);t.select()}}function m(){setTimeout(function(){l()},0)}function g(){var e=parseFloat("0")/Math.pow(10,t.precision);return e.toFixed(t.precision).replace(new RegExp("\\.","g"),t.decimal)}function y(i){if(e.browser.msie){p(i)}if(n.val()===""||n.val()===u(g())){if(!t.allowZero){n.val("")}else if(!t.affixesStay){n.val(g())}else{n.val(u(g()))}}else{if(!t.affixesStay){var s=n.val().replace(t.prefix,"").replace(t.suffix,"");n.val(s)}}if(n.val()!==r){n.change()}}function b(){var e=n.get(0),t;if(e.setSelectionRange){t=n.val().length;e.setSelectionRange(t,t)}else{n.val(n.val())}}var n=e(this),r;t=e.extend(t,n.data());n.unbind(".maskMoney");n.bind("keypress.maskMoney",p);n.bind("keydown.maskMoney",d);n.bind("blur.maskMoney",y);n.bind("focus.maskMoney",v);n.bind("click.maskMoney",b);n.bind("cut.maskMoney",m);n.bind("paste.maskMoney",m);n.bind("mask.maskMoney",l)})}};e.fn.maskMoney=function(n){if(t[n]){return t[n].apply(this,Array.prototype.slice.call(arguments,1))}else if(typeof n==="object"||!n){return t.init.apply(this,arguments)}else{e.error("Method "+n+" does not exist on jQuery.maskMoney")}}})(window.jQuery)


/* FORM PATTERNS ------------------------------ */

function setFormPatterns()
{
	// Prevent changing the hidden fields in the execution time
	$('input[type=hidden]').each(function () { $(this).attr('readonly','readonly'); });
	
	$('input').unmask();				
	
	// Mask patterns
	$('input[data-mask=money]').each(function ()           { $(this).maskMoney({prefix:'R$ ', allowNegative: true, thousands:'.', decimal:',', affixesStay: false}); });		
	$('input[data-mask=dateTime]').each(function ()        { $(this).mask('99/99/9999 99:99:99') });
	$('input[data-mask=time]').each(function ()            { $(this).mask('99:99') });
	$('input[data-mask=date]').each(function ()            { $(this).mask('99/99/9999'); });
	$('input[data-mask=cpf]').each(function ()             { $(this).mask('999.999.999-99') });
	$('input[data-mask=cnpj]').each(function ()            { $(this).mask('99.999.999/9999-99') });
	$('input[data-mask=cep]').each(function ()             { $(this).mask('99999-999') });
	
	$('input[data-mask=phone]').each(function () 
	{
		$(this).focusout(function(){
			var phone, element;
			element = $(this);
			element.unmask();
			phone = element.val().replace(/\D/g, '');
			if(phone.length > 10) {
				element.mask("(99) 99999-999?9");
			} else {
				element.mask("(99) 9999-9999?9");
			}
		}).trigger('focusout');		
	});
}


/* SET BVALIDATOR ------------------------------ */

function setValidator()
{
	$('form[bValidator]').submit(function () {
		
		var formId = $(this).attr('id');
		
		$("#"+formId).bValidator({validateOn: 'blur', position: {x:'center', y:'center'}, offset:{x:10,y:0}});
		
		if(!$('#'+formId).data('bValidator').validate())
		{
			alert('Alguns campos obrigatórios não foram preenchidos. Favor verificar e corrigir');
			return false;
		}
		else
		{
			$('#'+formId+' input[type=submit]').val('Aguarde...');
			$('#'+formId+' input[type=submit]').attr('disabled',true);
			return true;
		}
		
	});
}


/* MOEDA -> FLOAT ------------------------------ */

function moeda2float(moeda)
{

   moeda = moeda.replace(".","");
   moeda = moeda.replace(",",".");

   return parseFloat(moeda);

}


/* FLOAT -> MOEDA ------------------------------ */

function float2moeda(num) 
{

   x = 0;

   if(num<0) {
      num = Math.abs(num);
      x = 1;
   }
   if(isNaN(num)) num = "0";
      cents = Math.floor((num*100+0.5)%100);

   num = Math.floor((num*100+0.5)/100).toString();

   if(cents < 10) cents = "0" + cents;
      for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
         num = num.substring(0,num.length-(4*i+3))+'.'
               +num.substring(num.length-(4*i+3));
   ret = num + ',' + cents;
   if (x == 1) ret = ' - ' + ret;
   
   return ret;

}


/* CHAR COUNT ------------------------------ */

function charCount(fieldId,limit,toDiv)
{
	
	$('#'+fieldId).keyup(function(e) 
	{

		var charLength = $(this).val().length;

		$('#'+toDiv).html(charLength + ' de <strong>'+limit+'</strong> caracteres usados');

		if($(this).val().length > limit) 
		{
	
			$(this).val($(this).val().substr(0,limit));
			$('#'+toDiv).html('<strong>Limite de caracteres atingido</strong>');

		}
		
	});
	
}